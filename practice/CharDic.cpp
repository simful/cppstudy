#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

class CharDic {
public:

    //用数字生成种子
    void Nmake_small_char_list() {
        int n = 0;
        cout << "输入数字代表前N个字符,请输入:" << endl;
        cin >> n;
        cout << "ready..." << endl;
        for (int i = 0; i < n; i++) {
            small_char_list += std::string(1, char_list[i]);
        }
    }

    //用自己的字符串生成种子
    void Cmake_small_char_list() {
        cout << "输入字符串代表你要生成字典的种子字符,请输入:";
        cout << "请输入:";
        cout << "ready..." << endl;
        cin >> small_char_list;
    }

    //生成字典
    void get_dic() {
        for (char &c1: small_char_list) {
            for (char &c2: small_char_list) {
                for (char &c3: small_char_list) {
                    for (char &c4: small_char_list) {
                        for (char &c5: small_char_list) {
                            for (char &c6: small_char_list) {
                                dic.push_back(std::string(1, c1) +
                                              std::string(1, c2) +
                                              std::string(1, c3) +
                                              std::string(1, c4) +
                                              std::string(1, c5) +
                                              std::string(1, c6));
                            }
                        }
                    }
                }
            }
        }
    }


    //输出字典
    void display_dic() {
        for (string &item: dic) {
            cout << item << endl;
        }
    }

    //字典输出到文件
    void dic_to_file(const string &filename) {
        ofstream outfile(filename, ios::app);
        if (outfile.is_open()) {
            for (string &item: dic) {
                outfile << item << endl;
            }
            outfile.close();
        } else {
            cout << "can not open file.";
        }
    }

private:
    //定义字典
    vector<string> dic;
    //定义遍历字典，初始化为abcdef
    string small_char_list = "abcdef";
    //总的种子库
    char char_list[27] = "abcdefghijklmnopqrstuvwxyz";
};

int main() {
    CharDic animal;
    animal.Nmake_small_char_list();
    animal.get_dic();
    animal.display_dic();
    return 0;
}