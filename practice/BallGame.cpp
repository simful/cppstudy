#include <iostream>
#include <graphics.h>
#include <cstdlib>
#include <ctime>

const int SCR_W = 1024;					//屏幕窗口宽度
const int SCR_H = 500;					//屏幕窗口高度
const int MAP_W = SCR_W * 2;			//地图宽度
const int MAP_H = SCR_H * 2;			//地图高度
const int FD_NUM = 200;                 //食物小球数量
const int AI_NUM = 40;                  //AI小球数量
const double SPF = 1000 / 80;           //每帧图像绘制所需的毫秒数

//创建小球结构体
struct Ball 
{
	double x, y;						//小球圆点坐标，决定小球在地图位置
	double r;                           //半径，决定小球的大小
	bool flag;                          //决定小球是否存活，true-1存活，false-0消亡
	COLORREF color;						//小球的填充颜色
};

Ball FD_Ball[FD_NUM];                   //创建FD_NUM个食物小球
Ball PL_Ball;                           //创建玩家小球
Ball AI_Ball[AI_NUM];					//创建AI小球
POINT REF_XY;                           //屏幕相对位置

void SCR_Draw();						//绘制屏幕
void FD_Draw();							//绘制食物小球
void PL_Draw();							//绘制玩家小球
void AI_Draw();							//绘制AI小球
void MAP_Draw();						//绘制地图
void GAME_Draw();						//绘制游戏
void FD_Init();							//食物小球初始化
void PL_Init();							//玩家小球初始化
void AI_Init();							//AI小球初始化
void GAME_Init();						//游戏初始化
void PL_Control(int step);				//玩家小球控制
void PL_Eat_FD();						//玩家小球吃食物小球
void AI_Eat_FD();						//AI小球吃食物小球
void Camera_Pos();						//获取将视角定位到以玩家小球为中心的相对坐标
double CC_Distance(Ball b1, Ball b2);	//计算两个球之间的距离
void FD_Add(int i);						//补充食物小球
void AI_AutoMove();                     //AI小球自动移动
void AI_Catch(Ball* AI_Catch, Ball* AI_Run, int step);  //AI小球追击
void AI_Eat_AI(Ball* AI_Catch, Ball* AI_Run);   //AI小球吃掉AI小球

IMAGE MAP(MAP_W, MAP_H);				//创建地图

int main() 
{
	
	initgraph(SCR_W, SCR_H,SHOWCONSOLE);//绘制窗口
	GAME_Init();						//游戏初始化
	while (true)
	{
		clock_t Start_Draw_Time = clock();    //记录绘制开始的时钟节拍
		GAME_Draw();                    //绘制游戏
		clock_t Frame_Draw_Time = clock() - Start_Draw_Time;   //记录绘制结束时花费时钟数
		if (Frame_Draw_Time < SPF)
		{
			clock_t DelayTime = SPF - Frame_Draw_Time;
			Sleep(DelayTime);
		}
		PL_Control(2);                  //玩家小球控制
		PL_Eat_FD();					//玩家小球吃食物小球
		AI_AutoMove();                  //AI小球自动移动
	}
	
	std::cin.get();
	return 0;
}

//绘制屏幕
void SCR_Draw() 
{
	Camera_Pos();
	SetWorkingImage();								//设置屏幕窗口为当前绘制设备
	putimage(0, 0, SCR_W, SCR_H, &MAP, REF_XY.x, REF_XY.y);		//加载图片
}

//绘制地图
void MAP_Draw() 
{
	SetWorkingImage(&MAP);							//设置地图为当前绘制设备
	setbkcolor(WHITE);								//设置当前图片背景色
	cleardevice();									//使用当前设置背景色清空图片
}

//绘制食物小球
void FD_Draw()
{
	for (int i = 0; i < FD_NUM; i++)
	{
		if (FD_Ball[i].flag)
		{
			setfillcolor(FD_Ball[i].color);							//设置小球填充色，如果不设置就用的是地图背景色
			solidcircle(FD_Ball[i].x, FD_Ball[i].y, FD_Ball[i].r);  //绘制实心食物小球
		}
	}
}

//绘制玩家小球
void PL_Draw() {
	if (PL_Ball.flag)
	{
		setfillcolor(PL_Ball.color);							//设置小球填充色，如果不设置就用的是地图背景色
		solidcircle(PL_Ball.x, PL_Ball.y, PL_Ball.r);           //绘制实心玩家小球
		
		setbkmode(TRANSPARENT);															//设置文本框透明色
		settextcolor(RED);																//设置名称颜色
		settextstyle(17, 0, _T("Consolas"));											//设置名称字体，大小
		outtextxy(PL_Ball.x - 30, PL_Ball.y - PL_Ball.r, _T("xiaofan"));            //添加玩家小球名称
	}
}

//绘制AI小球
void AI_Draw()
{
	for (int i = 0; i < AI_NUM; i++)
	{
		if (AI_Ball[i].flag)
		{
			setfillcolor(AI_Ball[i].color);							//设置小球填充色，如果不设置就用的是地图背景色
			solidcircle(AI_Ball[i].x, AI_Ball[i].y, AI_Ball[i].r);  //绘制实心食物小球
		}
	}
}

//绘制游戏
void GAME_Draw() 
{
	
	MAP_Draw();
	FD_Draw();
	PL_Draw();
	AI_Draw();
	SCR_Draw();
}

//食物小球初始化
void FD_Init() 
{
	for (int i = 0; i < FD_NUM; i++)
	{
		FD_Ball[i].x = std::rand() % MAP_W;													//随机设置食物小球圆心x  [0,MAP_w)
		FD_Ball[i].y = std::rand() % MAP_H;													//随机设置食物小球圆心y  [0,MAP_H)
		FD_Ball[i].r = std::rand() % 10 + 1;												//随机设置食物小球半径r  [0,10]
		FD_Ball[i].flag = true;																//小球初始设置为存活
		FD_Ball[i].color = RGB(std::rand() % 256,std::rand() % 256,std::rand() % 256);      //设置小球初始颜色
	}
}

//玩家小球初始化
void PL_Init()
{
	PL_Ball.x = std::rand() % MAP_W;													//随机设置玩家小球圆心x  [0,SCR_w)
	PL_Ball.y = std::rand() % MAP_H;													//随机设置玩家小球圆心y  [0,SCR_H)
	PL_Ball.r = 13;																		//随机设置玩家小球半径r  13
	PL_Ball.flag = true;																//小球初始设置为存活
	PL_Ball.color = GREEN;																//设置小球初始颜色 绿色
}

//AI小球初始化
void AI_Init()
{
	for (int i = 0; i < AI_NUM; i++)
	{
		AI_Ball[i].x = std::rand() % MAP_W;													//随机设置AI小球圆心x  [0,MAP_w)
		AI_Ball[i].y = std::rand() % MAP_H;													//随机设置AI小球圆心y  [0,MAP_H)
		AI_Ball[i].r = std::rand() % 20 + 10;												//随机设置AI小球半径r  [20,29]
		AI_Ball[i].flag = true;																//小球初始设置为存活
		AI_Ball[i].color = RGB(std::rand() % 256, std::rand() % 256, std::rand() % 256);    //设置小球初始颜色,随机
	}
}

//游戏初始化
void GAME_Init()
{
	std::srand(time(0));                  //生成随机种子
	FD_Init();							  //食物小球初始化
	PL_Init();							  //玩家小球初始化
	AI_Init();							  //AI小球初始化
}

//玩家小球控制
void PL_Control(int step) {
	//获取异步按键状态(玩家小球上移一步）
	if (GetAsyncKeyState(VK_UP))
	{
		if (PL_Ball.y - PL_Ball.r > 0)
		{
			PL_Ball.y -= step;
		}	
	}

	//获取异步按键状态(玩家小球下移一步）
	if (GetAsyncKeyState(VK_DOWN))
	{
		if (PL_Ball.y + PL_Ball.r < MAP_H)
		{
			PL_Ball.y += step;
		}
	}

	//获取异步按键状态(玩家小球左移一步）
	if (GetAsyncKeyState(VK_LEFT))
	{
		if (PL_Ball.x - PL_Ball.r > 0)
		{
			PL_Ball.x -= step;
		}
	}
	//获取异步按键状态(玩家小球右移一步）
	if (GetAsyncKeyState(VK_RIGHT))
	{
		if (PL_Ball.x + PL_Ball.r < MAP_W)
		{
			PL_Ball.x += step;
		}
	}
}

//获取将视角定位到以玩家小球为中心的相对坐标
void Camera_Pos()
{
	REF_XY.x = PL_Ball.x - SCR_W / 2;
	REF_XY.y = PL_Ball.y - SCR_H / 2;

	if (REF_XY.x < 0) REF_XY.x = 0;
	if (REF_XY.y < 0) REF_XY.y = 0;
	if (REF_XY.x > MAP_W - SCR_W) REF_XY.x = MAP_W - SCR_W;
	if (REF_XY.y > MAP_H - SCR_H) REF_XY.y = MAP_H - SCR_H;
}
//计算两个球之间的距离
double CC_Distance(Ball b1, Ball b2) {
	return sqrt((b1.x - b2.x) * (b1.x - b2.x) + (b1.y - b2.y) * (b1.y - b2.y));
}


//判断小球生存或消亡
void PL_Eat_FD()
{
	int i;
	for (i = 0; i < FD_NUM; i++)
	{
		if (CC_Distance(PL_Ball, FD_Ball[i]) <= PL_Ball.r && FD_Ball[i].flag)
		{
			FD_Ball[i].flag = false;
			PL_Ball.r += FD_Ball[i].r * 0.1;
		}
		//吃完食物小球后立即补充
		if (!FD_Ball[i].flag) FD_Add(i);
	}
}

//补充食物小球
void FD_Add(int i) 
{
	FD_Ball[i].x = std::rand() % MAP_W;													//随机设置食物小球圆心x  [0,MAP_w)
	FD_Ball[i].y = std::rand() % MAP_H;													//随机设置食物小球圆心y  [0,MAP_H)
	FD_Ball[i].r = std::rand() % 10 + 1;												//随机设置食物小球半径r  [0,10]
	FD_Ball[i].flag = true;																//小球初始设置为存活
	FD_Ball[i].color = RGB(std::rand() % 256, std::rand() % 256, std::rand() % 256);    //设置小球初始颜色
}

//AI小球自动移动
void AI_AutoMove()
{
	int index = -1;

	for (int i = 0; i < AI_NUM; i++)
	{
		int scale = MAP_W;
		int indexj = -1;
		int indexm = -1;
		if (AI_Ball[i].flag)
		{
			for (int j = 0; j < AI_NUM; j++)
			{
				if (i == j) continue;
				if (AI_Ball[i].r > AI_Ball[j].r && AI_Ball[j].flag)
				{
					if (CC_Distance(AI_Ball[i], AI_Ball[j]) < scale)
					{
						scale = CC_Distance(AI_Ball[i], AI_Ball[j]);
						index = j;
					}
				}
			}
		}

		if (index != -1)
		{
			AI_Catch(&AI_Ball[i], &AI_Ball[index], 1);
		}

	}
	
}
//AI小球追击
void AI_Catch(Ball* AI_Catch, Ball* AI_Run, int step)
{
	if (AI_Catch->x < AI_Run->x)
	{
		AI_Catch->x += step;
		AI_Eat_FD();
		AI_Eat_AI(AI_Catch, AI_Run);
	}
	else
	{
		AI_Catch->x -= step;
		AI_Eat_FD();
		AI_Eat_AI(AI_Catch, AI_Run);
	}
	if (AI_Catch->y < AI_Run->y)
	{
		AI_Catch->y += step;
		AI_Eat_FD();
		AI_Eat_AI(AI_Catch, AI_Run);
	}
	else
	{
		AI_Catch->y -= step;
		AI_Eat_FD();
		AI_Eat_AI(AI_Catch, AI_Run);
	}
}

//AI小球吃食物小球
void AI_Eat_FD()
{
	for (int i = 0; i < AI_NUM; i++) 
	{
		for (int j = 0; j < FD_NUM; j++) 
		{
			if (CC_Distance(AI_Ball[i],FD_Ball[j]) < AI_Ball[i].r && FD_Ball[j].flag)
			{
				FD_Ball[j].flag = false;
				AI_Ball[i].r += FD_Ball[j].r / 10;
			}
			//吃完食物小球后立即补充
			if (!FD_Ball[j].flag) FD_Add(j);
		}
	}
}

//AI小球吃掉AI小球
void AI_Eat_AI(Ball* AI_Catch, Ball* AI_Run)
{
	if (CC_Distance(*AI_Catch, *AI_Run) < AI_Catch->r && AI_Run->flag)
	{
		AI_Run->flag = false;
		AI_Catch->r += AI_Run->r / 10;
	}
}